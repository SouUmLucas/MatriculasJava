Escrever uma aplicação OO que solicita os seguintes dados:
 - Um nome de um curso
 - O número de pessoas interessados no curso
 - O nome de cada pessoa interessa no curso

Em seguida, o programa deve exibir uma mensagem seguindo a seguinte regra:
 - O curso será oferecido se existirem ao menos 50 pessoas interessdas. Nesse
   caso exibir "O curso será oferecido". Caso o contrário, "O curso não será
   oferecido"
 - O preço do curso por aluno é igual a R$10.000,00 dividido pelo número de alunos
   interessados no curso. Exibir o preço médio se o curso for oferecido
 - Exibir o número de alunos interessados

O nome de alunos não poderá ser repetidos